import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'labs/lab1.dart';
import 'labs/lab2.dart';
import 'labs/lab3.dart';
import 'labs/lab6.dart';
import 'labs/lab7.dart';
// import 'labs/lab8.dart';

Widget drawer(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Text(
              'Лабораторные работы',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
        ),
        ListTile(
            leading: Icon(Icons.brush_outlined),
            title: Text('Элементы GUI'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => GUIElements()));
            }),
        ListTile(
            leading: Icon(Icons.play_circle_outline),
            title: Text('Фото и видео'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => VideoPlayerApp()));
            }),
        ListTile(
            leading: Icon(Icons.http),
            title: Text('http/https запросы'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => HttpRequestsLab()));
            }),
        ListTile(
            leading: Icon(Icons.lock),
            title: Text('Шифрование данных'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => EncDecLab()));
            }),
        ListTile(
            leading: Icon(Icons.android),
            title: Text('Чат-сервер на WebSocket'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => WebsocketLab()));
            }),
      ],
    ),
  );
}
