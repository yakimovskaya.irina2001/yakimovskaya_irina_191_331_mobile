import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../drawer.dart';

class GUIElements extends StatefulWidget {
  const GUIElements({Key? key}) : super(key: key);

  @override
  _GUIElementsState createState() => _GUIElementsState();
}

enum SingingCharacter { no, yes }

class _GUIElementsState extends State<GUIElements> {
  RangeValues _currentRangeValues = const RangeValues(40, 80);
  // final TextEditingController _text = TextEditingController();
  SingingCharacter _character = SingingCharacter.no;
  double _value = 20;
  bool isChecked = false;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.blue;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("Элементы GUI"),
      ),
      drawer: drawer(context),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              SleekCircularSlider(
                  min: 0,
                  max: 100,
                  initialValue: 80,
                  appearance: CircularSliderAppearance(
                    infoProperties: InfoProperties(modifier: (double value) {
                      // функция должна возвращать String
                      final roundedValue = value.ceil().toInt().toString();
                      return roundedValue;
                    }),
                    customColors: CustomSliderColors(
                      gradientStartAngle: 0,
                      gradientEndAngle: 180,
                      trackColor: Color.fromRGBO(0, 69, 107, 0.1),
                      progressBarColors: <Color>[
                        // Color.fromRGBO(150, 0, 221, 100),
                        // Color.fromRGBO(255, 41, 0, 100),
                        // Color.fromRGBO(255, 153, 0, 100),
                        // Color.fromRGBO(255, 0, 184, 100),
                        // Color.fromRGBO(255, 255, 255, 100),
                        // Color.fromRGBO(2, 136, 209, 100),
                        Colors.white70,
                        Colors.blue
                      ],
                    ),
                    angleRange: 300,
                    startAngle: 120,
                  ),
                  onChange: (double value) {
                    // print(value);
                  }),
              Padding(
                padding: const EdgeInsets.all(20),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration(
                    labelText: 'Данные',
                    filled: true,
                    fillColor: Colors.white12,
                  ),
                ),
              ),
              RangeSlider(
                values: _currentRangeValues,
                min: 0,
                max: 100,
                divisions: 100,
                labels: RangeLabels(
                  _currentRangeValues.start.round().toString(),
                  _currentRangeValues.end.round().toString(),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    _currentRangeValues = values;
                  });
                },
              ),
              Column(
                children: <Widget>[
                  ListTile(
                    title: Text('Нет', style: TextStyle(fontSize: 18)),
                    leading: Radio<SingingCharacter>(
                      fillColor:
                          MaterialStateProperty.all<Color>(Color(0xff0288d1)),
                      value: SingingCharacter.no,
                      groupValue: _character,
                      onChanged: (SingingCharacter? value) {
                        setState(() {
                          _character = value!;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: Text('Да', style: TextStyle(fontSize: 18)),
                    leading: Radio<SingingCharacter>(
                      fillColor:
                          MaterialStateProperty.all<Color>(Color(0xff0288d1)),
                      value: SingingCharacter.yes,
                      groupValue: _character,
                      onChanged: (SingingCharacter? value) {
                        setState(() {
                          _character = value!;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Row(
                  children: [
                    Container(
                      width: width - 128,
                      child: Text("Прочитал и на все согласен (согласна)",
                          style: TextStyle(fontSize: 18)),
                    ),
                    Checkbox(
                      checkColor: Colors.white,
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: isChecked,
                      onChanged: (bool? value) {
                        setState(() {
                          isChecked = value!;
                        });
                      },
                    )
                  ],
                ),
              ),
              Slider(
                value: _value,
                onChanged: (double value) {
                  setState(() {
                    _value = value;
                  });
                },
                min: 0,
                max: 100,
                divisions: 100,
                label: _value.round().toString(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
