import 'package:html/parser.dart' show parse;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_html/flutter_html.dart';
import '../drawer.dart';

String data = '';
String htmlData = "";

class HttpRequestsLab extends StatefulWidget {
  const HttpRequestsLab({Key? key}) : super(key: key);

  @override
  _HttpRequestsLabState createState() => _HttpRequestsLabState();
}

class _HttpRequestsLabState extends State<HttpRequestsLab> {
  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('http/https запросы'),
        actions: <Widget>[
          Row(
            children: [
              IconButton(
                icon: const Icon(Icons.refresh),
                onPressed: () {
                  _loadData();
                },
              ),
            ],
          )
        ],
      ),
      drawer: drawer(context),
      body: SingleChildScrollView(
        child: Container(
          height: height - 100,
          child: Column(
            children: [
              Container(
                height: height - 148,
                child: PageView(
                  children: <Widget>[
                    Container(
                      child: SingleChildScrollView(
                          child: Column(
                        children: [
                          Html(data: htmlData),
                        ],
                      )),
                    ),
                    Container(
                      child: SingleChildScrollView(child: Text(htmlData)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: TextField(
                  style: TextStyle(fontSize: 14, color: Colors.black87),
                  enabled: false,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    labelText: "Температура сегодня: $data",
                    focusColor: Colors.blueAccent,
                    fillColor: Colors.white10,
                    filled: true,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _loadData() async {
    // print("Was invoked");
    Uri uri = Uri.parse('https://sinoptik.com.ru/погода-ейск');
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      var document = response.body;
      // print(document);
      // print((parse(document)
      //     .getElementsByClassName("weather__content_tab-temperature")[0]
      //     .text));
      setState(() {
        htmlData = document.toString();
        data = parse(document)
            .getElementsByClassName("weather__content_tab-temperature")[0]
            .text
            .replaceAll("\n", ' ');
      });
    }
  }
}
