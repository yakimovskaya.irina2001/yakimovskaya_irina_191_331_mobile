import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:encrypt/encrypt.dart' as Enc;
import '../drawer.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'dart:convert';

String generateKey(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

class EncDecLab extends StatefulWidget {
  EncDecLab({Key? key}) : super(key: key);
  @override
  _EncDecLabState createState() => _EncDecLabState();
}

class _EncDecLabState extends State<EncDecLab> {
  final keyField = TextEditingController();
  final textField = TextEditingController();

  @override
  void dispose() {
    keyField.dispose();
    textField.dispose();
    super.dispose();
  }

  Future<String> encrypt(String keyStr) async {
    final plainText = textField.text;
    keyStr = generateKey(keyStr);
    print("Ключ: $keyStr");
    final key = Enc.Key.fromUtf8(keyStr);
    final iv = Enc.IV.fromLength(16);
    try {
      final encrypter = Enc.Encrypter(Enc.AES(key)).encrypt(plainText, iv: iv);

      String encrypted = encrypter.base64;

      print("Зашифрованное: $encrypted");
      await _write(encrypted);
      return encrypted;
    } catch (error) {
      return "Exception";
    }
  }

  Future<String> decrypt(String keyStr) async {
    final cipherText = await _read();
    if (cipherText != 'None') {
      keyStr = generateKey(keyStr);

      print("Ключ: $keyStr");
      final key = Enc.Key.fromUtf8(keyStr);
      final iv = Enc.IV.fromLength(16);

      final encrypter = Enc.Encrypter(Enc.AES(key));
      final encryptedText = Enc.Encrypted.fromBase64(cipherText);
      try {
        String decrypted = encrypter.decrypt(encryptedText, iv: iv);
        print("Расшифрованное: $decrypted");
        return decrypted;
      } catch (e) {
        return "Exception"; // неверный ключ
      }
    } else {
      print("Couldn't read file");
      return "Exception";
    }
  }

  Future<void> _write(String text) async {
    try {
      Directory directory = await getApplicationDocumentsDirectory();
      print(directory.path);
      File file = File('${directory.path}/my_file.txt');
      await file.writeAsString(text);
    } catch (e) {
      print("Couldn't write in file");
    }
  }

  Future<String> _read() async {
    String text;
    try {
      Directory directory = await getApplicationDocumentsDirectory();
      File file = File('${directory.path}/my_file.txt');
      text = await file.readAsString();
      return text;
    } catch (e) {
      print("Couldn't read file");
      return "None";
    }
    // print('');
    // print(text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: drawer(context),
      appBar: AppBar(
        title: Text("Шифрование данных"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Container(
                      height: 60,
                      child: TextField(
                        // obscureText: true,
                        controller: keyField,
                        decoration: InputDecoration(
                          suffixIcon: Icon(Icons.vpn_key_outlined),
                          labelText: 'Введите ключ',
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: TextField(
                      controller: textField,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      decoration: InputDecoration(
                        suffixIcon: Icon(Icons.sticky_note_2_rounded),
                        labelText: 'Введите текст',
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextButton(
                          onPressed: () async {
                            final enc = await encrypt(keyField.text);
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Center(child: Text("Шифрование")),
                                  content: SingleChildScrollView(
                                    child: Text(enc),
                                  ),
                                );
                              },
                            );
                          },
                          child: Text(
                            'Зашифровать в файл',
                            style: TextStyle(color: Colors.white),
                          ),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.blue),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextButton(
                          onPressed: () async {
                            final dec = await decrypt(keyField.text);
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                    title: Center(child: Text("Расшифрование")),
                                    content: SingleChildScrollView(
                                        child: Text(dec)));
                              },
                            );
                          },
                          child: Text(
                            'Расшифровать из файла',
                            style: TextStyle(color: Colors.black87),
                          ),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                side: BorderSide(
                                  color: Colors.blue,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
