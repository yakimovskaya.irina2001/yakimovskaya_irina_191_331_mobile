import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import '../drawer.dart';
import 'package:intl/intl.dart';

class Message {
  late String text;
  late String time;
  late bool sender;

  Message(_text, _time, _sender) {
    text = _text;
    time = _time;
    sender = _sender;
  }
}

class WebsocketLab extends StatefulWidget {
  WebsocketLab({Key? key}) : super(key: key);
  @override
  _WebsocketLabState createState() => _WebsocketLabState();
}

class _WebsocketLabState extends State<WebsocketLab> {
  @override
  Widget build(BuildContext context) {
    List<Message> loadedMessages = [];
    final TextEditingController _controller = TextEditingController();
    ScrollController _scrollController = ScrollController();
    final _channel = WebSocketChannel.connect(
      Uri.parse('ws://yakim-websockets-app.herokuapp.com/'),
    );

    void _sendMessage() {
      if (_controller.text.isNotEmpty) {
        _channel.sink.add(_controller.text);
        String now = DateFormat('kk:mm').format(DateTime.now());
        loadedMessages.add(Message(_controller.text, now, true));
        _controller.clear();
      }
    }

    return Scaffold(
      drawer: drawer(context),
      appBar: AppBar(
        title: Text("Чат-сервер на WebSocket"),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/backgr.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            StreamBuilder(
                stream: _channel.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var dataList = jsonDecode(snapshot.data.toString());
                    print(dataList);
                    loadedMessages.add(
                        Message(dataList['content'], dataList['time'], false));
                  }
                  return Container(
                    constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height - 140,
                    ),
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: loadedMessages.length,
                      shrinkWrap: false,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      itemBuilder: (context, index) {
                        if (loadedMessages[index].sender) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                loadedMessages[index].time,
                                style: TextStyle(color: Colors.white),
                              ),
                              Bubble(
                                padding: BubbleEdges.all(10.0),
                                margin: BubbleEdges.only(top: 15.0),
                                nip: BubbleNip.rightBottom,
                                child: Column(children: <Widget>[
                                  ConstrainedBox(
                                      constraints: BoxConstraints(
                                        maxWidth:
                                            MediaQuery.of(context).size.width -
                                                80,
                                      ),
                                      child: Text(loadedMessages[index].text,
                                          style:
                                              TextStyle(color: Colors.white))),
                                ]),
                                alignment: Alignment.centerRight,
                                color: Color(0xFF5B6F87),
                              ),
                            ],
                          );
                        } else {
                          return Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Bubble(
                                padding: BubbleEdges.all(10.0),
                                margin: BubbleEdges.only(top: 15.0),
                                nip: BubbleNip.leftBottom,
                                child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                      maxWidth:
                                          MediaQuery.of(context).size.width -
                                              80,
                                    ),
                                    child: Text(loadedMessages[index].text,
                                        style: TextStyle(color: Colors.white))),
                                alignment: Alignment.centerLeft,
                                color: Color(0xFF364A63),
                              ),
                              Text(loadedMessages[index].time,
                                  style: TextStyle(color: Colors.white)),
                            ],
                          );
                        }
                      },
                    ),
                  );
                }),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                height: 60,
                width: double.infinity,
                color: Color(0xFF33495F),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        onSubmitted: (String str) => _sendMessage(),
                        controller: _controller,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Введите сообщение",
                            hintStyle: TextStyle(color: Colors.white),
                            border: InputBorder.none),
                      ),
                    ),
                    FloatingActionButton(
                      backgroundColor: Color(0xFF33495F),
                      elevation: 0,
                      focusElevation: 0,
                      hoverElevation: 0,
                      highlightElevation: 0,
                      disabledElevation: 0,
                      onPressed: () => _sendMessage(),
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                        size: 25,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
